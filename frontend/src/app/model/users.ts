export class Users {
    id: number;
    public email: string;
    public password: string;
    public firstname: string;
    public lastname: string;
    public active: number;

    constructor(id:number, email:string, password:string, firstname: string, lastname: string, active: number) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.active = active;
    }   
}