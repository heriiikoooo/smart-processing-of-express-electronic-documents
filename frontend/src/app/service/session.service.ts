import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Users } from '../model/users';
import { Router } from '@angular/router';

import { md5 } from '../md5';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  vID = md5('id');
  id = null;

  redirectUrl: string;

  private currentUserSubject: BehaviorSubject<Users>;
  public currentUser: Observable<Users>;
  
  loggedInStatus = false;

  constructor(
	  private router: Router
	  ) { }

  public get currentUserValue(): Users {
		return this.currentUserSubject.value;
  }

  setToken(token: string) {
		localStorage.setItem(this.vID, md5(token));
	}

	getToken() {
		return localStorage.getItem(this.vID);
	}

	deleteToken() {
		localStorage.removeItem(this.vID);
		localStorage.removeItem('id');
		// localStorage.removeItem('user_route');
	}

	isLoggedIn() {
		const usertoken = this.getToken();
		this.id = localStorage.getItem('id');
		// console.log(this.id)
		if (usertoken != null) {
			return true
		}
		return false;
	}
	

	getSessionLogin(){ 
		if(this.isLoggedIn()) {
			var user_route = localStorage.getItem('user_route');
			if(this.router.url == user_route +'/login'){
				this.router.navigate(['home']);
      		}
		}
	}
}
