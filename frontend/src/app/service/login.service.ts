import { Injectable, Output, EventEmitter } from '@angular/core';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';

@Injectable({
	providedIn: 'root'
})
export class LoginService {
	PHP_API_SERVER = GlobalVariable.BASE_API_URL;;
	ID: number;
	@Output() getLoggedInName: EventEmitter<any> = new EventEmitter();
	constructor(
		private httpClient: HttpClient,
	) { }

	public userlogin(username: string, password: string, userType: number) {
		return this.httpClient.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/User/login.php', { username, password, userType })
			.pipe(
				catchError(this.handleError)
			);
	}

	private handleError(err: HttpErrorResponse) {
		let errorMessage = '';
		if (err.error instanceof ErrorEvent) {
			errorMessage = `An error occurred: ${err.error.message}`;
		} else {
			errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
		}
		console.error(errorMessage);
		return throwError(errorMessage);
	}

}
