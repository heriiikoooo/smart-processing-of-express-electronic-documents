import { Injectable, Output, EventEmitter } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';
import { SessionService } from './session.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;;

  constructor(
    private http: HttpClient,
    private sessionService: SessionService) { 

  }
  addUser(email: string,
    userType: number,
    active: boolean,
    specialization: number,
    firstName: string,
    lastName: string,
    address: string,
    city: string,
    zipCode: number,
    password: string,
   ) {
    return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/User/addUser.php', {
      email, userType, active, specialization, firstName, lastName, address, city, zipCode, password
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  updateUser(
    id: number,
    email: string,
    userType: number,
    active: boolean,
    specialization: number,
    firstName: string,
    lastName: string,
    address: string,
    city: string,
    zipCode: number,
    password: string,
   ) {
    return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/User/updateUser.php', {
      id, email, userType, active, specialization, firstName, lastName, address, city, zipCode, password
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  public getAllUsers(): Observable<any> {
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/User/getAllUsers.php'),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  public getUserById(id: number){
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/User/user.php/?id='+id),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  public getUserDetails(): Observable<any> {
    // console.log(this.sessionService.id)
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/User/user.php/?id='+this.sessionService.id),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  public deleteUserByID(id: number){
		return this.http.delete<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/User/deleteUser.php/?id='+id),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

}
