import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';
import { SessionService } from './session.service'

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;;

  constructor(
    private http: HttpClient,
    private sessionService: SessionService) { 

  }

  getAllSpecializations(): Observable<any> {
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Specialization/getAllSpecializations.php'),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  getAvSpecializations(): Observable<any> {
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Specialization/getAvSpecializations.php'),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  public getSpecializationById(id: number){
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Specialization/getSpecializationById.php/?id='+id),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  public addSpecialization(
    name: string
  ) {
    console.log(name)
    return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/Specialization/addSpecialization.php', {
      name
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  updateSpecialization(
    id: number,
    name: string
  ) {
    console.log(id)
    console.log(name)
    return this.http.put<any>(this.PHP_API_SERVER + '/FUNCTIONS/Specialization/updateSpecialization.php', {
      id, name
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

}
