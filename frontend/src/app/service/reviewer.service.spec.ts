/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ReviewerService } from './reviewer.service';

describe('Service: Reviewer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReviewerService]
    });
  });

  it('should ...', inject([ReviewerService], (service: ReviewerService) => {
    expect(service).toBeTruthy();
  }));
});
