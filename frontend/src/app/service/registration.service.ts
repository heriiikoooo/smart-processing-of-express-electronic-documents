import { Injectable } from '@angular/core';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;;
  constructor(private httpClient : HttpClient) { }

  public userregistration(
    email: string,
    firstName: string,
    lastName: string,
    address: string,
    city: string,
    zipCode: number,
    password: string,
    userType: number) {
    return this.httpClient.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/User/register.php', {
      email, firstName, lastName, address, city, zipCode, password, userType
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
		let errorMessage = '';
		if (err.error instanceof ErrorEvent) {
		  errorMessage = `An error occurred: ${err.error.message}`;
		} else {
		  errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
		}
		console.error(errorMessage);
		return throwError(errorMessage);
	  }

}
