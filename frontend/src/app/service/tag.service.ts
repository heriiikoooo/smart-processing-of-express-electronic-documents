import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';
import { SessionService } from './session.service'

@Injectable({
  providedIn: 'root'
})
export class TagService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;;

constructor(
  private http: HttpClient,
) { }

  getAllTags(): Observable<any>{
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Tag/getAllTags.php'), {

    }).pipe(
        catchError(this.handleError)
      );
  }

  getAvTags(): Observable<any> {
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Tag/getAvTags.php'),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }

  getTagById(id: number): Observable<any>{
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/Tag/getTagById.php/?id='+id),{
      
    }).pipe(
      catchError(this.handleError)
    )
  }

  public addTag(
    name: string
  ) {
    console.log(name)
    return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/Tag/addTag.php', {
      name
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  updateTag(
    id: number,
    name: string
  ) {
    return this.http.put<any>(this.PHP_API_SERVER + '/FUNCTIONS/Tag/updateTag.php', {
      id, name
      })
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

}
