import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  constructor() { }

  getLinksNormal() {
    return [
      { path: '/dashboard', title: 'Dashboard', icon: 'nc-bank', class: '', iconType: 'nc' },
      { path: '/icons', title: 'Icons', icon: 'nc-diamond', class: '', iconType: 'nc' },
      { path: '/maps', title: 'Maps', icon: 'nc-pin-3', class: '', iconType: 'nc' },
      { path: '/notifications', title: 'Notifications', icon: 'nc-bell-55', class: '', iconType: 'nc' },
      { path: '/user', title: 'User Profile', icon: 'nc-single-02', class: '', iconType: 'nc' },
      { path: '/table', title: 'Table List', icon: 'nc-tile-56', class: '', iconType: 'nc' },
      { path: '/typography', title: 'Typography', icon: 'nc-caps-small', class: '', iconType: 'nc' },
      { path: '/upgrade', title: 'Upgrade to PRO', icon: 'nc-spaceship', class: 'active-pro', iconType: 'nc' },
      { path: '/users', title: 'Manage Users', icon: 'nc-single-02', class: '', iconType: 'nc' },
      { path: '/research-papers', title: 'Research Papers', icon: 'fa-file-text-o', class: '', iconType: 'fa' },
    ];
  }

  getnoUserRoutes() {
    return [
      { path: '/researcher/login', title: 'Authors Portal', icon: 'fa-pencil', class: '', iconType: 'fa' },
      { path: '/reviewer/login', title: 'Referees Portal', icon: 'fa-commenting-o', class: '', iconType: 'fa' },
      { path: '/admin/login', title: 'Research and Planning Office', icon: 'nc-circle-10', class: '', iconType: 'nc' },
    ];
  }

  getadminRoutes() {
    return [
      { path: '/admin/research-papers', title: 'Research Papers', icon: 'fa-file-text-o', class: '', iconType: 'fa' },
      { path: '/admin/users', title: 'Manage Users', icon: 'nc-single-02', class: '', iconType: 'nc' },
      { path: '/admin/specializations', title: 'Specializations / Tags', icon: 'fa-certificate', class: '', iconType: 'fa' },
    ];
  }

  getresearcherRoutes() {
    return [
      { path: '/researcher/research-papers', title: 'Research Paper', icon: 'fa-file-text-o', class: '', iconType: 'fa' },
    ];
  }

  getreviewerRoutes() {
    return [
      { path: '/reviewer/research-papers', title: 'Review Research Paper', icon: 'fa-pencil-square-o', class: '', iconType: 'fa' },
    ];
  }
}
