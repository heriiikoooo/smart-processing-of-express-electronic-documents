/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ResearchPaperService } from './research-paper.service';

describe('Service: ResearchPaper', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResearchPaperService]
    });
  });

  it('should ...', inject([ResearchPaperService], (service: ResearchPaperService) => {
    expect(service).toBeTruthy();
  }));
});
