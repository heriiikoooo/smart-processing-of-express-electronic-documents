import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';

@Injectable({
  providedIn: 'root'
})
export class ResearchPaperService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;


constructor(
  private http: HttpClient
) { }

public getAllResearchPapers(): Observable<any> {
  return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/research-paper/getAllResearchPaper.php'),  {
    
  }).pipe(
    catchError(this.handleError)
  );
}

public getOwnResearchPapers(id: number){
  return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/research-paper/getOwnResearchPapers.php?id='+id),  {
    
  }).pipe(
    catchError(this.handleError)
  );
}

public getResearchPaperById(id: number){
  return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/research-paper/getResearchPaperById.php?id='+id),  {
    
  }).pipe(
    catchError(this.handleError)
  );
}

addPaper(
  title: string,
  body: string,
  tags: [],
  userID: number,
  status: string
 ) {
  return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/research-paper/addPaper.php', {
    title, body, tags, userID, status
    })
  .pipe(
    catchError(this.handleError)
  );
}

updatePaper(
  id: number,
  title: string,
  body: string,
  tags: string,
  userID: number,
  status: string
 ) {
  return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/research-paper/updatePaper.php', {
    id, title, body, tags, userID, status
    })
  .pipe(
    catchError(this.handleError)
  );
}

deleteResearchPaperByID(id: number){
  return this.http.delete<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/research-paper/deleteResearchPaper.php/?id='+id),  {
      
  }).pipe(
    catchError(this.handleError)
  );
}

setReviewer(
  id: number,
  reviewer_id: number,
 ) {
  return this.http.post<any>(this.PHP_API_SERVER + '/FUNCTIONS/research-paper/setReviewer.php', {
    id, reviewer_id,
    })
  .pipe(
    catchError(this.handleError)
  );
}

private handleError(err: HttpErrorResponse) {
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
}

}
