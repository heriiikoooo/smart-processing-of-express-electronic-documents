import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GlobalVariable } from '../server';

@Injectable({
  providedIn: 'root'
})
export class ReviewerService {
  PHP_API_SERVER = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  public getAllAssignedResearchPapers(id: number){
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/reviewer/getAllAssignedResearchPapers.php?id='+id),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }
  
  public getActiveReviewers(): Observable<any> {
    return this.http.get<any>(this.PHP_API_SERVER.concat('/FUNCTIONS/reviewer/getActiveReviewer.php'),  {
      
    }).pipe(
      catchError(this.handleError)
    );
  }
  
  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
