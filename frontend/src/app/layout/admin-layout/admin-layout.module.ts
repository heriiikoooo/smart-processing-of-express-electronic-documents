import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent }            from '../../pages/home/home.component';
// import { DashboardComponent }       from '../../pages/dashboard/dashboard.component';
import { UserComponent }            from '../../pages/user/user.component';
import { TableComponent }           from '../../pages/table/table.component';
import { TypographyComponent }      from '../../pages/typography/typography.component';
import { IconsComponent }           from '../../pages/icons/icons.component';
import { MapsComponent }            from '../../pages/maps/maps.component';
// import { NotificationsComponent }   from '../../pages/notifications/notifications.component';
import { UpgradeComponent }         from '../../pages/upgrade/upgrade.component';

import { AlertService } from '../../service/alert.service'
import { UsersComponent } from '../../pages/admin/users/users/users.component';
import { UserFormComponent } from '../../pages/admin/users/user-form/user-form.component';

import { ResearchPaperComponent } from '../../pages/admin/research-paper/research-paper.component';
import { ResearchPaperListComponent } from '../../pages/researcher/research-paper-list/research-paper-list.component'
import { ResearchPaperFormComponent } from '../../pages/researcher/research-paper-form/research-paper-form.component';

import { ReviewerResearchPaperListComponent } from '../../pages/reviewer/reviewer-research-paper-list/reviewer-research-paper-list.component';
import { ReviewerFormComponent } from '../../pages/reviewer/reviewer-form/reviewer-form.component';
import { MyProfileComponent } from '../../pages/my-profile/my-profile/my-profile.component';

import { SpecializationsComponent } from '../../pages/admin/specializations/specializations/specializations.component';
import { SpecializationFormComponent } from '../../pages/admin/specializations/specialization-form/specialization-form.component';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CKEditorModule,
    TagInputModule,
    // BrowserAnimationsModule
  ],
  declarations: [
    HomeComponent,
    // DashboardComponent,
    UserComponent,
    TableComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    // NotificationsComponent,
    UsersComponent,
    UserFormComponent,
    ResearchPaperComponent,
    ResearchPaperListComponent,
    ResearchPaperFormComponent,
    ReviewerResearchPaperListComponent,
    ReviewerFormComponent,
    SpecializationsComponent,
    SpecializationFormComponent,
    MyProfileComponent
  ]
})

export class AdminLayoutModule {}
