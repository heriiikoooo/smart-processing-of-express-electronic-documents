import { Routes } from '@angular/router';

// import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import {HomeComponent} from '../../pages/home/home.component'
import { UserComponent } from '../../pages/user/user.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
// import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';
// import { ProductsComponent } from '../../component/products/products.component';
import { UsersComponent } from '../../pages/admin/users/users/users.component';
import { UserFormComponent } from '../../pages/admin/users/user-form/user-form.component'
import { ResearchPaperComponent } from '../../pages/admin/research-paper/research-paper.component';
import { ResearchPaperListComponent } from '../../pages/researcher/research-paper-list/research-paper-list.component'
import { ResearchPaperFormComponent } from '../../pages/researcher/research-paper-form/research-paper-form.component';
import { ReviewerResearchPaperListComponent } from '../../pages/reviewer/reviewer-research-paper-list/reviewer-research-paper-list.component';
import { ReviewerFormComponent } from '../../pages/reviewer/reviewer-form/reviewer-form.component';
import { SpecializationsComponent } from '../../pages/admin/specializations/specializations/specializations.component';
import { SpecializationFormComponent } from '../../pages/admin/specializations/specialization-form/specialization-form.component';
import { MyProfileComponent } from '../../pages/my-profile/my-profile/my-profile.component';

import {AuthguardGuard } from '../../authguard.guard';
import { AdminGuard } from '../../guard/admin.guard';
import { ResearcherGuard } from '../../guard/researcher.guard';
import { ReviewerGuard } from '../../guard/reviewer.guard';



export const AdminLayoutRoutes: Routes = [
    { path: '/',      component: HomeComponent },
    { path: 'home',      component: HomeComponent },
    // { path: 'dashboard',      component: DashboardComponent },
    { path: 'admin/my-profile',           component: MyProfileComponent,canActivate: [AuthguardGuard, AdminGuard] },
    { path: 'researcher/my-profile',           component: MyProfileComponent,canActivate: [AuthguardGuard, ResearcherGuard] },
    { path: 'reviewer/my-profile',           component: MyProfileComponent,canActivate: [AuthguardGuard] },

    { path: 'admin/users', component: UsersComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/users/add', component: UserFormComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/users/edit/:id', component: UserFormComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/research-papers', component: ResearchPaperComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/specializations', component: SpecializationsComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/specializations/add', component: SpecializationFormComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    { path: 'admin/specializations/edit/:id', component: SpecializationFormComponent,   canActivate: [AuthguardGuard, AdminGuard]},
    
    { path: 'admin/user',           component: UserComponent,canActivate: [AuthguardGuard] },
    { path: 'admin/table',          component: TableComponent,canActivate: [AuthguardGuard] },
    { path: 'admin/typography',     component: TypographyComponent,canActivate: [AuthguardGuard, ResearcherGuard] },
    { path: 'admin/icons',          component: IconsComponent,canActivate: [AuthguardGuard, AdminGuard] },
    { path: 'admin/maps',           component: MapsComponent, canActivate: [AuthguardGuard] },
    // { path: 'notifications',  component: NotificationsComponent },
    { path: 'admin/upgrade',        component: UpgradeComponent },
    { path: 'researcher/research-papers',component: ResearchPaperListComponent,canActivate: [AuthguardGuard, ResearcherGuard] },
    { path: 'researcher/research-papers/add', component: ResearchPaperFormComponent, canActivate: [AuthguardGuard, ResearcherGuard]},
    { path: 'researcher/research-papers/edit/:id', component: ResearchPaperFormComponent, canActivate: [AuthguardGuard, ResearcherGuard]},
    { path: 'reviewer/research-papers', component: ReviewerResearchPaperListComponent, canActivate: [AuthguardGuard, ReviewerGuard]},
    { path: 'reviewer/research-papers/review/:id', component: ReviewerFormComponent, canActivate: [AuthguardGuard, ReviewerGuard]}
    // { path: 'php-angular',        component: UpgradeComponent }
];
