import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from  './component/login/login.component';
import { ProductsComponent } from './component/products/products.component';
import { SidebarModule } from './component/sidebar/sidebar.module';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { NavbarModule} from './component/shared/navbar/navbar.module';
import { RegistrationComponent } from  './component/registration/registration.component';
import { AlertComponent } from './component/alert/alert.component';
import { NotAuthorizedComponent } from './component/error/not-authorized/not-authorized.component';
import { AddComponent } from './pages/researcher/papers/add/add.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { ResearchPaperListComponent } from './pages/researcher/research-paper-list/research-paper-list.component';
// import { AlertService }

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    ProductsComponent,
    RegistrationComponent,
    AlertComponent,
    NotAuthorizedComponent,
    // ResearchPaperListComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SidebarModule,
    NavbarModule,
    CKEditorModule,
    TagInputModule,
    BrowserAnimationsModule
  ],
  exports: [AlertComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
