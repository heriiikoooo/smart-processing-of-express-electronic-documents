import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import { UserService } from '../../../../service/user.service'
import { AlertService } from '../../../../service/alert.service'
import {SpecializationService } from '../../../../service/specialization.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  moduleId: module.id,
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any[];
  user_specialization: any = []
  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private specializationService: SpecializationService
){}

  ngOnInit() {
    this.getAllUsers()
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(
      result => {
        this.users = result['result'];
        for(var i = 0; i < this.users.length; i++){
          if(this.users[i]['user_type'] == 3){
            var uID = this.users[i]['id'];
            this.getUserSpecializationById(uID, this.users[i]['specialization']);
          }
        }
      }
    );
  }

  getUserSpecializationById(uID, id){
    this.specializationService.getSpecializationById(id).subscribe(
      result => {
        this.user_specialization[uID] = result['result'][0]['name']
      }
    );
  }

  deleteUser(id){
		this.userService.deleteUserByID(id).subscribe(
      result=>{
        console.log(result);
        alert("User deleted successfully")
			// console.log("user deleted, ", user);
      this.getAllUsers();
    },

    error => {
      // this.alertService.error(error);
      console.log(error)
      });
	}

}
