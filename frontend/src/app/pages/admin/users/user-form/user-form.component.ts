import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PasswordMatch } from '../../../../helpers/password-match.validator';
import { UserService } from '../../../../service/user.service';
import { AlertService } from '../../../../service/alert.service'
import {SpecializationService } from '../../../../service/specialization.service'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  angForm: FormGroup;
  public users: any;
  id: any;
  buttonState: any;
  usertype: any = ['Admin', 'Researcher', 'Reviewer'];
  user_specialization: number;
  avSpecializations: any;
  user_type: number;
  isArchived: any;
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private specializationService: SpecializationService
    ) {
      this.angForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.minLength(1), Validators.email]],
      userType: ['', Validators.required],
      specialization: ['',],
      active: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: PasswordMatch('password', 'confirmPassword')
    });
  }

  ngOnInit() {
    console.log(this);
    this.route.params.subscribe(params => {
      this.id = params['id'];
      if(this.id != null){
        console.log(this.id)
        this.buttonState = true;
        this.getUserById(this.id);
      }
      else{
        this.buttonState = false;
      }
      
    });
    this.getAvSpecializations();
    
  }

  getUserById(id: number){
    this.userService.getUserById(id).subscribe(
        result => {
            this.users = result['result'][0];
            this.isArchived = this.users.archived;
            this.user_type = this.users['user_type'];
            if(this.users['user_type'] == 3){
              
              this.specializationService.getSpecializationById(this.users['specialization']).subscribe(
                result => {
                  console.log( result['result'][0]['id'])
                  this.user_specialization = result['result'][0]['id']
                }
              );
              
            }
            
            
            console.log(this.users);
        },
        error => {
          
        }
      );
  }

  add(angForm1){
    var userType;
    var active;
    console.log('add')
    console.log(angForm1)
    if(angForm1.value.userType == 'Admin'){
      userType = 1;
    }
    else if(angForm1.value.userType == 'Researcher'){
      userType = 2;
    }
    else if(angForm1.value.userType == 'Reviewer'){
      userType = 3;
    }
    this.isArchived = angForm1.value.active ? 'f' : 't'
    console.log(angForm1)
    this.userService.addUser(
      angForm1.value.email,
      userType,
      this.isArchived,
      angForm1.value.specialization,
      angForm1.value.firstName,
      angForm1.value.lastName,
      angForm1.value.address,
      angForm1.value.city,
      angForm1.value.zipCode,
      angForm1.value.password
      )
    .pipe(first())
    .subscribe(
    data => {
      console.log(data);
      if(data['status'] == true){
        this.router.navigate(['admin/users']);
      }
      else{
        alert('Registration Failed')
      }
    },
  
    error => {
      });
  }

  removeValidators(){ 
    console.log('removing validators')
    this.angForm.get('password').clearValidators();
    this.angForm.get('password').updateValueAndValidity();

    this.angForm.get('confirmPassword').clearValidators();
    this.angForm.get('confirmPassword').updateValueAndValidity();
  }

  isTouched(){
    this.angForm.get('confirmPassword').setValidators([Validators.required, Validators.minLength(6)]);
    this.angForm.get('confirmPassword').updateValueAndValidity();
    this.angForm.get('password').setValidators([Validators.required, Validators.minLength(6)]);
    this.angForm.get('password').updateValueAndValidity();

    

  }

  update(angForm1){
    console.log(angForm1)
    var userType;
    console.log('update')
    console.log(this.isArchived)
    this.isArchived = this.isArchived ? 'f' : 't';
    if(angForm1.value.active ==  undefined){
      this.isArchived = this.users['archived']
    }
    console.log(this.isArchived)
    console.log(angForm1.value)
    if(angForm1.value.userType == 'Admin'){
      userType = 1;
    }
    else if(angForm1.value.userType == 'Researcher'){
      userType = 2;
    }
    else if(angForm1.value.userType == 'Reviewer'){
      userType = 3;
    }

    this.userService.updateUser(
      this.id,
      angForm1.value.email,
      userType,
      this.isArchived,
      angForm1.value.specialization,
      angForm1.value.firstName,
      angForm1.value.lastName,
      angForm1.value.address,
      angForm1.value.city,
      angForm1.value.zipCode,
      angForm1.value.password
      )
    .pipe(first())
    .subscribe(
    data => {
      console.log(data);
      if(data['status'] == true){
        this.router.navigate(['admin/users']);
      }
      else{
        alert('Updating users Failed');
      }
    },
  
    error => {
      });
    

  }

  checkValue(event: any){
    this.isArchived = event.target.checked;
    console.log(this.isArchived)
 }

 changeWebsite(e) {

  console.log(e.target.value);
  if(e.target.value == 'Admin'){
    this.user_type = 1
  }
  else if(e.target.value == 'Researcher'){
    this.user_type = 2
  }
  else if(e.target.value == 'Reviewer'){
    this.user_type = 3
  }

}

getAvSpecializations(){
  this.specializationService.getAvSpecializations().subscribe(
    result => {
      console.log(result)
      this.avSpecializations = result['result'];
        console.log(this.avSpecializations);
    },
    error => {
      
    }
  );
}

  get email() { return this.angForm.get('email'); }
  get userType() { return this.angForm.get('userType'); }
  get specialization() { return this.angForm.get('specialization'); }
  get active() { return this.angForm.get('active'); }
  get password() { return this.angForm.get('password'); }
  get confirmPassword() { return this.angForm.get('confirmPassword'); }
  get firstName() { return this.angForm.get('firstName'); }
  get lastName() { return this.angForm.get('lastName'); }
  get address() { return this.angForm.get('address'); }
  get city() { return this.angForm.get('city'); }
  get zipCode() { return this.angForm.get('zipCode'); }
  get f() { return this.angForm.controls; }
}
