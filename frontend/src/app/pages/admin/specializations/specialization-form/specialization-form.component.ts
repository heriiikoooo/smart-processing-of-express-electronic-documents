import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import {SpecializationService } from '../../../../service/specialization.service'

@Component({
  selector: 'app-specialization-form',
  templateUrl: './specialization-form.component.html',
  styleUrls: ['./specialization-form.component.css']
})
export class SpecializationFormComponent implements OnInit {
  specializationForm: FormGroup;
  public specializations: any;
  id: any;
  buttonState: any;
  btnId: any;

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private formBuilder: FormBuilder,
    private specializationService: SpecializationService,
  ) {
    this.specializationForm = this.formBuilder.group({
      name: ['', Validators.required],
    });
   }

  ngOnInit() {
    console.log(this);
    this.route.params.subscribe(params => {
      this.id = params['id'];
      if(this.id != null){
        console.log(this.id)
        this.buttonState = true;
        this.getSpecializationById(this.id);
      }
      else{
        this.buttonState = false;
      }

      this.btnId = this.buttonState ? 'updatebtn' : 'addbtn';
      
    });
  }

  getSpecializationById(id: number){
    this.specializationService.getSpecializationById(id).subscribe(
        result => {
            this.specializations = result['result'][0];
        },
        error => {
          
        }
      );
  }

  add(form){
    console.log(form)
      // if (specializationForm.value.name.length > 0) {
      //   console.log('Already Exist')
      // }
      this.specializationService.addSpecialization(
        form.value.name
        )
      .pipe(first())
      .subscribe(
      data => {
        console.log(data);
        if(data['status'] == true){
          this.router.navigate(['admin/specializations']);
        }
        else{
          alert('Adding new Specialization Failed');
        }
      },
    
      error => {
        });
  }

  update(form){
    console.log('update')
    console.log(form)

    this.specializationService.updateSpecialization(
      this.id,
      form.value.name
      )
    .pipe(first())
    .subscribe(
    data => {
      console.log(data);
      if(data['status'] == true){
        this.router.navigate(['admin/specializations']);
      }
      else{
        alert('Updating Specialization Failed');
      }
    },
  
    error => {
      });
  }

  get f() { return this.specializationForm.controls; }
  get name() { return this.specializationForm.get('name'); }

}
