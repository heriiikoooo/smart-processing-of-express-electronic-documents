import { Component, OnInit } from '@angular/core';
import {SpecializationService } from '../../../../service/specialization.service'

@Component({
  selector: 'app-specializations',
  templateUrl: './specializations.component.html',
  moduleId: module.id,
  styleUrls: ['./specializations.component.css']
})
export class SpecializationsComponent implements OnInit {
  specializations: any = [];
  constructor(
    private specializationService: SpecializationService
  ) { }

  ngOnInit() {
    this.getAllSpecializations();
  }

  getAllSpecializations() {
    this.specializationService.getAllSpecializations().subscribe(
      result => {
        this.specializations = result['result'];
          console.log(this.specializations);
      },
      error => {
        
      }
    );
  }

}
