import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { ResearchPaperService } from '../../../service/research-paper.service';
import { ReviewerService } from '../../../service/reviewer.service';
import { SpecializationService } from '../../../service/specialization.service';

@Component({
  selector: 'app-research-paper',
  templateUrl: './research-paper.component.html',
  styleUrls: ['./research-paper.component.css']
})
export class ResearchPaperComponent implements OnInit {
  reviewerForm: FormGroup;
  researchPapers: any[];
  pickedResearchPaper: any;
  reviewers: any[];
  action: any;
  public reviewerState: any;
  researchPaperTag: any = []

  constructor(
    private modalService: NgbModal,
    private researchPaperService: ResearchPaperService,
    private reviewerService: ReviewerService,
    private formBuilder: FormBuilder,
    private router:Router,
    private tagService: SpecializationService,
  ) { 
    this.reviewerForm = this.formBuilder.group({
      reviewer: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.researchPaperService.getAllResearchPapers().subscribe(
      result => {
        this.researchPapers = result['result'];
          console.log(this.researchPapers);
          this.action = this.researchPapers['reviewer_id'] != null ? 'Re-Assign to Reviewer' : 'Assign to Reviewer';
          for(var i = 0; i < this.researchPapers.length; i++){
            var uID = this.researchPapers[i]['id'];
            this.getResearchPaperTagById(uID, this.researchPapers[i]['tag_id']);
        }
      },
      error => {
        
      }
    );
  }

  getResearchPaperTagById(uID, id){
    this.tagService.getSpecializationById(id).subscribe(
      result => {
        if(result['result'] != null){
          this.researchPaperTag[uID] = result['result'][0]['name']
        }
        else{
          this.researchPaperTag[uID] = "";
        }
        
      }
    );
  }

  get f() { return this.reviewerForm.controls; }
  get reviewer() { return this.reviewerForm.get('reviewer'); }
}
