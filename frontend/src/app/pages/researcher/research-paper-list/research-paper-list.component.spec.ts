import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchPaperListComponent } from './research-paper-list.component';

describe('ResearchPaperListComponent', () => {
  let component: ResearchPaperListComponent;
  let fixture: ComponentFixture<ResearchPaperListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchPaperListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchPaperListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
