import { Component, OnInit } from '@angular/core';
import { ResearchPaperService } from '../../../service/research-paper.service';
import { SessionService } from '../../../service/session.service';
import { SpecializationService } from '../../../service/specialization.service';

@Component({
  selector: 'app-research-paper-list',
  templateUrl: './research-paper-list.component.html',
  styleUrls: ['./research-paper-list.component.css']
})
export class ResearchPaperListComponent implements OnInit {
  researchPapers: any[];
  researchPaperTag: any = []
  
  constructor(
    private researchPaperService: ResearchPaperService,
    private sessionService: SessionService,
    private tagService: SpecializationService
  ) { }

  ngOnInit() {
    this.getOwnResearchPapers();
  }

  getOwnResearchPapers() {
    console.log(this.sessionService.id)
    this.researchPaperService.getOwnResearchPapers(this.sessionService.id).subscribe(
      result => {
        this.researchPapers = result['result'];
          for(var i = 0; i < this.researchPapers.length; i++){
              var uID = this.researchPapers[i]['id'];
              this.getResearchPaperTagById(uID, this.researchPapers[i]['tag_id']);
          }
      },
      error => {
        
      }
    );
  }

  getResearchPaperTagById(uID, id){
    this.tagService.getSpecializationById(id).subscribe(
      result => {
        if(result['result'] != null){
          this.researchPaperTag[uID] = result['result'][0]['name']
        }
        else{
          this.researchPaperTag[uID] = "";
        }
        
      }
    );
  }

  deleteUser(id){
		this.researchPaperService.deleteResearchPaperByID(id).subscribe(
      result=>{
        console.log(result);
        alert("Research Paper deleted successfully")
      this.getOwnResearchPapers();
    },

    error => {
      console.log(error)
      });
	}

}
