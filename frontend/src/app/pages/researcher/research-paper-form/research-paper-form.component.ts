import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import * as ClassicEditor from '../../../../assets/ckeditor/ckeditor-with-comment.js';

import { ResearchPaperService } from '../../../service/research-paper.service';
import { SessionService } from '../../../service/session.service';
import { SpecializationService } from '../../../service/specialization.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-research-paper-form',
  templateUrl: './research-paper-form.component.html',
  styleUrls: ['./research-paper-form.component.css']
})
export class ResearchPaperFormComponent implements OnInit {
  @ViewChild("sidebar", { static: true }) sidebarEl: ElementRef<any>;
  researchForm: FormGroup;
  public researchPaper: any;
  id: any;
  buttonState: any;
  public status = 'Pending';
  public Editor = ClassicEditor;

  get f() { return this.researchForm.controls; }
  get title() { return this.researchForm.get('title'); }
  get body() { return this.researchForm.get('body'); }
  get tags() { return this.researchForm.get('tags'); }

  public ckeditor_status = false;

  public config = {
    // commentsOnly: true,
    users: [],

    sidebar: {
      container: '',
    },
    toolbar: {
      items: [
        'heading',
        '|',
        'bold',
        'italic',
        'underline',
        'link',
        'bulletedList',
        'numberedList',
        'fontBackgroundColor',
        'fontColor',
        'fontFamily',
        'fontSize',
        '|',
        'alignment',
        '|',
        'indent',
        'outdent',
        '|',
        'codeBlock',
        'imageUpload',
        'blockQuote',
        'insertTable',
        'mediaEmbed',
        'imageInsert',
        'removeFormat',
        'specialCharacters',
        'subscript',
        'strikethrough',
        'superscript',
        'exportPdf',
        'exportWord',
        'undo',
        'redo'
      ]
    },
    language: 'en',
    image: {
      toolbar: [
        'imageTextAlternative',
        'imageStyle:full',
        'imageStyle:side',
        'linkImage'
      ]
    },
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells',
        'tableCellProperties',
        'tableProperties'
      ]
    },
    simpleUpload: {
      // The URL that the images are uploaded to.
      uploadUrl: 'http://localhost/maam-kim-thesis/api/FUNCTIONS/Uploads/uploadfile.php',

      // Enable the XMLHttpRequest.withCredentials property.
      withCredentials: false,

    },
    // licenseKey: 'sDsCdxFvjYAqvmat8eMnwqTsgYxAXX9FCAHPvrFYZKzi3dRIVMu5BSxW8peJ',
    licenseKey: 'lh/6MUHW54ljeSCYbDbtHuJq7wSSDG5rYbliurEUQdjcVyplTQ==',
  }


  public sidebar = {
    sidebar: {
      container: '#sidebar'
    },
  }
  public defaultConfig: any;

  tag: any = [];
  avTags = [];
  user_type: number;
  public currentUser: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private researchPaperService: ResearchPaperService,
    private sessionService: SessionService,
    private tagService: SpecializationService,
    private userService: UserService
  ) {
    this.researchForm = this.formBuilder.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
      tags: ['', Validators.required],
    });
  }

  ngOnInit() {
    console.log(this.sidebarEl)
    console.log(this.config)
    this.config.sidebar.container = this.sidebarEl.nativeElement;

    this.getUserDetails();
    console.log(this.currentUser)

    // this.config= 

    this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        console.log(this.id)
        this.buttonState = true;
        this.getResearchPaperById(this.id);
      }
      else {
        this.buttonState = false;
      }

    });
    this.getAvTags()
  }


  getUserDetails() {
    this.userService.getUserDetails().subscribe(
      result => {
        this.currentUser = result['result'][0];
        console.log(this.currentUser)
        this.config.users = [{ id: this.currentUser.id, name: this.currentUser.first_name + ' ' + this.currentUser.last_name }];
        console.log(this.config)
        
        this.user_type = this.currentUser['user_type'];
        console.log(this.user_type);
        this.ckeditor_status = true;
      },
      error => {

      }
    )
  }

  public onReady(editors) {
    console.log(editors);
    editors.plugins.get('Users').addUser(this.config.users[0]);
    editors.plugins.get('Users').defineMe(this.config.users[0].id);

    const annotationsUIs = editors.plugins.get('AnnotationsUIs');
    annotationsUIs.switchTo('narrowSidebar');
    const commentsRepositoryPlugin = editors.plugins.get( 'CommentsRepository' );
    

    // for

    commentsRepositoryPlugin.adapter = {
      addComment( data ) {
          return Promise.resolve( {
              createdAt: new Date()
          } );
      },

      updateComment( data ) {
          return Promise.resolve();
      },

  //     removeComment( data ) {
  //         return Promise.resolve();
  //     },

      getCommentThread( data ) {
        console.log(this.researchPaper)
        console.log(editors)
        console.log(data)
          return Promise.resolve( {
              threadId: data.threadId,
              comments: [
                  {
                      commentId: '',
                      authorId: editors.plugins.get('Users').me.id,
                      dataCommentId: 'e57f54a15873dbc6001922275e843dbe1',
                      content: data.threadId,
                      date: new Date()
                  }
              ],
              isFromAdapter: true
          } );
      }
  };

  }

  getResearchPaperById(id: number) {
    this.researchPaperService.getResearchPaperById(id).subscribe(
      result => {
        this.researchPaper = result['result'][0];
        this.tagService.getSpecializationById(this.researchPaper['tag_id']).subscribe(
          result => {
            if (result['result'] != null) {
              var id = result['result'][0]['id'];
              var displayData = result['result'][0]['name']
              if (result['result'][0]['name'] != "") {
                this.tag = [{ id: id, display: displayData }];
              }
              else {
                this.tag = [];
              }
            }
            else {
              this.tag = [];
            }
          });

        this.status = this.researchPaper.research_paper_status;
      },
      error => {

      }
    );
  }

  getAvTags() {
    this.tagService.getAvSpecializations().subscribe(
      result => {
        this.avTags = result['result'];
      },
      error => {

      }
    );
  }

  add() {
    console.log('add')
    console.log(this.researchForm);

    this.researchPaperService.addPaper(
      this.researchForm.value.title,
      this.researchForm.value.body,
      this.researchForm.value.tags[0].id,
      this.sessionService.id,
      this.status)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data['status'] == true) {
            this.router.navigate(['researcher/research-papers']);
          }
          else {
            alert('Adding new Research Paper Failed')
          }
        },
        error => {
        });
  }

  update(researchForm) {
    console.log(researchForm)
    console.log(researchForm.value.tags[0].id)

    this.researchPaperService.updatePaper(
      this.id,
      researchForm.value.title,
      researchForm.value.body,
      researchForm.value.tags[0].id,
      this.sessionService.id,
      this.status)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data['status'] == true) {
            this.router.navigate(['researcher/research-papers']);
          }
          else {
            alert('Updating Research Paper Failed');
          }
        },

        error => {
        });
  }
}
