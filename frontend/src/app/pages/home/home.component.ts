import { Component } from '@angular/core';

@Component({
    selector: 'home',
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent{
    public currentUser;
    constructor () {
        this.currentUser = localStorage.getItem('currentUser')? JSON.parse(localStorage.getItem('currentUser')) : '';
        console.log(this.currentUser)
      }
    
      ngOnInit() {
      }
}
