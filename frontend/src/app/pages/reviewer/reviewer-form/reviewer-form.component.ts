import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import * as ClassicEditor from '../../../../assets/ckeditor/ckeditor-with-comment.js';

import { ResearchPaperService } from '../../../service/research-paper.service';
import { SessionService } from '../../../service/session.service';
import { SpecializationService } from '../../../service/specialization.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-reviewer-form',
  templateUrl: './reviewer-form.component.html',
  styleUrls: ['./reviewer-form.component.css']
})
export class ReviewerFormComponent implements OnInit {
  @ViewChild("sidebar", { static: true }) sidebarEl: ElementRef<any>;
  public reviewerForm: FormGroup;
  public researchPaper: any;
  public id: any;
  public status = 'Pending';
  public Editor = ClassicEditor;
  public isDone: any;

  get f() { return this.reviewerForm.controls; }
  get title() { return this.reviewerForm.get('title'); }
  get body() { return this.reviewerForm.get('body'); }
  get tags() { return this.reviewerForm.get('tags'); }
  get done() { return this.reviewerForm.get('done'); }

  public ckeditor_status = false;

  public config = {
    commentsOnly: true,
    users: [],

    sidebar: {
      container: '',
    },
    toolbar: {
      items: [
        'comment',
        '|',
      ]
    },
    language: 'en',
    image: {
      toolbar: [
        'imageTextAlternative',
        'imageStyle:full',
        'imageStyle:side',
        'linkImage'
      ]
    },
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells',
        'tableCellProperties',
        'tableProperties'
      ]
    },
    simpleUpload: {
      // The URL that the images are uploaded to.
      uploadUrl: 'http://localhost/maam-kim-thesis/api/FUNCTIONS/Uploads/uploadfile.php',

      // Enable the XMLHttpRequest.withCredentials property.
      withCredentials: false,

    },
    // licenseKey: 'sDsCdxFvjYAqvmat8eMnwqTsgYxAXX9FCAHPvrFYZKzi3dRIVMu5BSxW8peJ',
    licenseKey: 'lh/6MUHW54ljeSCYbDbtHuJq7wSSDG5rYbliurEUQdjcVyplTQ==',
  }

  tag: any = [];
  user_type: number;
  public currentUser: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private researchPaperService: ResearchPaperService,
    private sessionService: SessionService,
    private tagService: SpecializationService,
    private userService: UserService
  ) {
    this.reviewerForm = this.formBuilder.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
      tags: ['', Validators.required],
      done: ['',]
    });
  }

  ngOnInit() {
    console.log(this.sidebarEl)
    console.log(this.config)
    this.config.sidebar.container = this.sidebarEl.nativeElement;

    this.getUserDetails();
    console.log(this.currentUser)

    // this.config= 

    this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        this.getResearchPaperById(this.id);
      }

    });
  }

  getUserDetails() {
    this.userService.getUserDetails().subscribe(
      result => {
        this.currentUser = result['result'][0];
        this.config.users = [{ id: this.currentUser.id, name: this.currentUser.first_name + ' ' + this.currentUser.last_name }];
        
        this.user_type = this.currentUser['user_type'];
        this.ckeditor_status = true;
      },
      error => {

      }
    )
  }

  public onReady(editors) {
    console.log(editors)
    editors.plugins.get('Users').addUser(this.config.users[0]);
    editors.plugins.get('Users').defineMe(this.config.users[0].id);

    const annotationsUIs = editors.plugins.get('AnnotationsUIs');
    annotationsUIs.switchTo('narrowSidebar');

    console.log(this.config);

    const commentsRepositoryPlugin = editors.plugins.get( 'CommentsRepository' );

    console.log(editors.plugins.get( 'CommentsRepository' )._threads.get([0]))
    console.log(commentsRepositoryPlugin);
    // console.log(commentsRepositoryPlugin._threads.entries());

  //   for (let key of commentsRepositoryPlugin.getThreads()) {
  //     console.log('yess')
  //     console.log(key[0]);
  // }

    let map = new Map([
      [ "APPLE", 1 ],
      [ "ORANGE", 2 ],
      [ "MANGO", 3 ]
  ]);

  console.log(map.entries());
  for (let key of map.keys()) {
    console.log(key);
}
    

    // for
var createdAt;
    commentsRepositoryPlugin.adapter = {
      addComment( data ) {
        console.log(data)
        // createdAt = new Date()
        // console.log(createdAt)
          return Promise.resolve( {
              // createdAt: new Date()
          } );
      },

      updateComment( data ) {
          return Promise.resolve();
      },

  //     removeComment( data ) {
  //         return Promise.resolve();
  //     },

      getCommentThread( data ) {
        createdAt = 'Mon Feb 21 2021 17:31:39 GMT+0800 (Philippine Standard Time'
        console.log(data)
          return Promise.resolve( {
              threadId: data.threadId,
              comments: [
                  {
                      authorId: editors.plugins.get('Users').me.id,
                      content: data.threadId,
                      date: new Date(createdAt)
                  }
              ],
              isFromAdapter: true
          } );
      }
  };

  }

  getResearchPaperById(id: number) {
    this.researchPaperService.getResearchPaperById(id).subscribe(
      result => {
        this.researchPaper = result['result'][0];
        console.log(this.researchPaper)
        this.tagService.getSpecializationById(this.researchPaper['tag_id']).subscribe(
          result => {
            if (result['result'] != null) {
              var id = result['result'][0]['id'];
              var displayData = result['result'][0]['name']
              if (result['result'][0]['name'] != "") {
                this.tag = [{ id: id, display: displayData }];
              }
              else {
                this.tag = [];
              }
            }
            else {
              this.tag = [];
            }
          });

        this.status = this.researchPaper.research_paper_status;
      },
      error => {

      }
    );
  }

  public onChange( editor) {
    const data = editor;

    console.log( data );
}


  update() {
    console.log(this.reviewerForm.value)
    console.log( this.sidebarEl.nativeElement)

    this.status = this.isDone ? 'Done' : 'In-Review';
    if(this.reviewerForm.value.done ==  undefined){
      this.status = this.researchPaper.research_paper_status
    }
    console.log(this.status)
    var date = new Date();

    this.researchPaperService.updatePaper(
      this.id,
      this.reviewerForm.value.title,
      this.reviewerForm.value.body,
      this.reviewerForm.value.tags[0].id,
      this.researchPaper.user_id,
      this.status)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data['status'] == true) {
            this.router.navigate(['reviewer/research-papers']);
          }
          else {
            alert('Updating Research Paper Failed');
          }
        },

        error => {
        });
  }

  checkValue(event: any){
    this.isDone = event.target.checked;
    console.log(this.isDone)
 }

}
