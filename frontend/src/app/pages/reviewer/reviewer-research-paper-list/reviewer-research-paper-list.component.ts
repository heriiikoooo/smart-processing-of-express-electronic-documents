import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../service/session.service';
import { ReviewerService } from '../../../service/reviewer.service';
import { SpecializationService } from '../../../service/specialization.service';

@Component({
  selector: 'app-reviewer-research-paper-list',
  templateUrl: './reviewer-research-paper-list.component.html',
  styleUrls: ['./reviewer-research-paper-list.component.css']
})
export class ReviewerResearchPaperListComponent implements OnInit {
  researchPapers: any[];
  researchPaperTag: any = []

  constructor(
    private sessionService: SessionService,
    private reviewerService: ReviewerService,
    private tagService: SpecializationService,
  ) { }

  ngOnInit() {
    this.getOwnResearchPapers();
  }

  getOwnResearchPapers() {
    console.log(this.sessionService.id)
    this.reviewerService.getAllAssignedResearchPapers(this.sessionService.id).subscribe(
      result => {
        this.researchPapers = result['result'];
          console.log(this.researchPapers);
          for(var i = 0; i < this.researchPapers.length; i++){
            var uID = this.researchPapers[i]['id'];
            this.getResearchPaperTagById(uID, this.researchPapers[i]['tag_id']);
        }
      },
      error => {
        
      }
    );
  }

  getResearchPaperTagById(uID, id){
    this.tagService.getSpecializationById(id).subscribe(
      result => {
        if(result['result'] != null){
          this.researchPaperTag[uID] = result['result'][0]['name']
        }
        else{
          this.researchPaperTag[uID] = "";
        }
        
      }
    );
  }

}
