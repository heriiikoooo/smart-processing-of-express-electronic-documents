import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PasswordMatch } from '../../../helpers/password-match.validator';

import { UserService } from '../../../service/user.service';
import { SessionService } from '../../../service/session.service';
import { SpecializationService } from '../../../service/specialization.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  myProfileForm: FormGroup;
  public user: any;
  public user_type: any;
  public user_specialization: any;
  public usertype: any = ['Admin', 'Researcher', 'Reviewer'];
  public avSpecializations: any;

  get f() { return this.myProfileForm.controls; }
  get email() { return this.myProfileForm.get('email'); }
  get userType() { return this.myProfileForm.get('userType'); }
  get specialization() { return this.myProfileForm.get('specialization'); }
  get password() { return this.myProfileForm.get('password'); }
  get confirmPassword() { return this.myProfileForm.get('confirmPassword'); }
  get firstName() { return this.myProfileForm.get('firstName'); }
  get lastName() { return this.myProfileForm.get('lastName'); }
  get address() { return this.myProfileForm.get('address'); }
  get city() { return this.myProfileForm.get('city'); }
  get zipCode() { return this.myProfileForm.get('zipCode'); }
  

  constructor(
    private router:Router,
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private userService: UserService,
    private specializationService: SpecializationService
  ) {
    this.myProfileForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.minLength(1), Validators.email]],
      userType: ['', Validators.required],
      specialization: ['',],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: PasswordMatch('password', 'confirmPassword')
    });
   }

  ngOnInit() {
    console.log(this.sessionService.id);
    this.getUserById(this.sessionService.id)
    this.getAvSpecializations()
  }

  getUserById(id: number){
    this.userService.getUserById(id).subscribe(
        result => {
            this.user = result['result'][0];
            this.user_type = this.user['user_type'];
            if(this.user_type == 3){
              console.log(this.user_type)
              console.log('reviewer')
              
              this.specializationService.getSpecializationById(this.user['specialization']).subscribe(
                result => {
                  console.log( result['result'][0]['id'])
                  this.user_specialization = result['result'][0]['id']
                }
              );
              
            }
            else{
              console.log('not rev')
            }
            
            
            console.log(this.user);
        },
        error => {
          
        }
      );
  }

  getAvSpecializations(){
    this.specializationService.getAvSpecializations().subscribe(
      result => {
        console.log(result)
        this.avSpecializations = result['result'];
          console.log(this.avSpecializations);
      },
      error => {
        
      }
    );
  }

  update(){
    console.log(this.myProfileForm)
    if(this.myProfileForm.invalid){
      return;
      // alert('Invalid')
    }
    else{}
    
  }

}
