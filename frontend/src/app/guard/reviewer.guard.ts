import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { SessionService } from '../service/session.service';
import { UserService } from '../service/user.service'

@Injectable({
  providedIn: 'root'
})
export class ReviewerGuard implements CanActivate {
  public user_type;
  public state = true;

  constructor(
    private sessionService: SessionService,
    private userService: UserService,
    private router: Router
    ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const routeurl: string = state.url;
      if(this.sessionService.isLoggedIn()){
        this.userService.getUserDetails().subscribe(
          result => {
            this.user_type = result['result'][0]['user_type'];
            console.log(this.user_type);
            if(this.user_type == 3){
              this.state = true;
              return this.state;
            }
            else{
              this.router.navigate(['/not-authorized']);
              console.log('NOT AUTHORIZED!!!')
              this.state = false;
              return this.state;
            }
          },
          error => {}
        );
      }
      return this.state;
  }
  
}
