import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RegistrationService } from '../../service/registration.service';
import { PasswordMatch } from '../../helpers/password-match.validator';
import { AlertService } from '../../service/alert.service'
import * as $ from 'jquery';

@Component({
  selector: 'app-register',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  angForm: FormGroup;
  user_route: string;
  user_type: number;
  constructor(
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService,
    private router:Router,
    private alertService: AlertService
    ) {
      this.angForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.minLength(1), Validators.email]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: PasswordMatch('password', 'confirmPassword')
    });
  }

  ngOnInit() {
    if(this.router.url == "/admin/registration"){
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/admin');
      this.user_type = 1;
    }
    if(this.router.url == "/researcher/registration"){
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/researcher');
      this.user_type = 2;
    }
    if(this.router.url == "/reviewer/registration"){
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/reviewer');
      this.user_type = 3;
    }
    this.user_route = localStorage.getItem('user_route');
  }

  postdata(angForm1)
  {
  this.registrationService.userregistration(
    angForm1.value.email,
    angForm1.value.firstName,
    angForm1.value.lastName,
    angForm1.value.address,
    angForm1.value.city,
    angForm1.value.zipCode,
    angForm1.value.password,
    this.user_type
    )
  .pipe(first())
  .subscribe(
  data => {
    if(data['status'] == true){
      console.log(data);
      setTimeout(() => {$(".alert").fadeIn(100) }, 100)
      this.alertService.success('Registration successful', true);
      setTimeout(() => {$(".alert").fadeOut(2000) }, 3000);
      setTimeout(() => { this.router.navigate([this.user_route + '/' + 'login']); }, 1500);
      // this.router.navigate(['login']);
    }
    else{
      setTimeout(() => {$(".alert").fadeIn(100) }, 100);
      this.alertService.error("Registration Failed", true);
      setTimeout(() => {$(".alert").fadeOut(2000) }, 3000);
    }
  },

  error => {
    this.alertService.error(error);
    });
  }

  get email() { return this.angForm.get('email'); }
  get password() { return this.angForm.get('password'); }
  get confirmPassword() { return this.angForm.get('confirmPassword'); }
  get firstName() { return this.angForm.get('firstName'); }
  get lastName() { return this.angForm.get('lastName'); }
  get address() { return this.angForm.get('address'); }
  get city() { return this.angForm.get('city'); }
  get zipCode() { return this.angForm.get('zipCode'); }
  get f() { return this.angForm.controls; }

}
