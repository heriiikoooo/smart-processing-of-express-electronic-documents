import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login.service';
import { SessionService } from '../../service/session.service';
import { AlertService } from '../../service/alert.service'
import * as $ from 'jquery';
import { md5 } from '../../md5';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  angForm: FormGroup;
  user_route: string;
  userType = null;

  constructor(private fb: FormBuilder,
    private loginService: LoginService,
    private sessionService: SessionService,
    private router: Router,
    private alertService: AlertService
  ) {
    this.angForm = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(1), Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.sessionService.getSessionLogin();
    if(this.router.url == "/admin/login" || this.router.url.indexOf('/admin/login') > -1){
      this.userType = 1;
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/admin');
    }
    if(this.router.url == "/researcher/login" || this.router.url.indexOf('/researcher/login') > -1){
      this.userType = 2;
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/researcher');
    }
    if(this.router.url == "/reviewer/login" || this.router.url.indexOf('/reviewer/login') > -1){
      this.userType = 3;
      localStorage.removeItem('user_route');
      localStorage.setItem('user_route', '/reviewer');
    }
    

    this.user_route = localStorage.getItem('user_route');
    console.log(this.user_route)
  }

  postdata() {
    this.loginService.userlogin(this.angForm.value.email, this.angForm.value.password, this.userType)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          const redirect = this.sessionService.redirectUrl ? this.sessionService.redirectUrl : '/home';
          if(data['status'] == true){
            console.log(data['result'])
            setTimeout(() => {$(".alert").fadeIn(100) }, 100)
            this.alertService.success('Login successful', true);
            setTimeout(() => {$(".alert").fadeOut(2000) }, 3000);
            localStorage.setItem('id',data['result'][0]['id']);
            this.sessionService.setToken(JSON.stringify(data['result'][0]['id']))
            setTimeout(() => { this.router.navigate([redirect]); }, 1500);
            
          }else{
            setTimeout(() => {$(".alert").fadeIn(100) }, 100);
            this.alertService.error("User name or password is incorrect", true);
            setTimeout(() => {$(".alert").fadeOut(2000) }, 3000);
          }
        },
        error => {
          this.alertService.error(error);
        });
  }

  ngOnDestroy(){

  }
  
  get f() { return this.angForm.controls; }
  get email() { return this.angForm.get('email'); }
  get password() { return this.angForm.get('password'); }

}
