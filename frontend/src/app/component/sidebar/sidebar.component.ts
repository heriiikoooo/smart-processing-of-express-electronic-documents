import { Component, OnInit } from '@angular/core';

import { UserService } from '../../service/user.service';
import { SessionService } from '../../service/session.service';
import { LinkService } from '../../service/link.service';
import { RouteInfo } from '../../model/routesinfo';

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public user_type;

    readonly noUserRoutes: RouteInfo[] = this.linkService.getnoUserRoutes();
    readonly adminRoutes: RouteInfo[] = this.linkService.getadminRoutes();
    readonly researcherRoutes: RouteInfo[] = this.linkService.getresearcherRoutes();
    readonly reviewerRoutes: RouteInfo[] = this.linkService.getreviewerRoutes();
    

    public user_route = localStorage.getItem('user_route');

    constructor(
        private userService: UserService,
        private sessionService: SessionService,
        private linkService: LinkService
    ){
        
    }

    ngOnInit() {
        
        if(this.sessionService.isLoggedIn()){
            this.getUserType()
        }
        else{
            this.menuItems = this.noUserRoutes.filter(menuItem => menuItem);
        }
    }

    getUserType(){
        this.userService.getUserDetails().subscribe(
            result => {
                console.log(result);
                this.user_type = result['result'][0]['user_type'];
                console.log(this.user_type);
                if(this.user_type == 1){
                    this.menuItems = this.adminRoutes.filter(menuItem => menuItem);
                }
                if(this.user_type == 2){
                    this.menuItems = this.researcherRoutes.filter(menuItem => menuItem);
                }
                if(this.user_type == 3){
                    this.menuItems = this.reviewerRoutes.filter(menuItem => menuItem);
                }
            },
            error => {
              
            }
          );


       

    }
}
