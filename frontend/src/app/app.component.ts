import { Component } from '@angular/core';
import { SessionService } from './service/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  loginbtn:boolean;
  logoutbtn:boolean;
  public currentUser;

  constructor(
    private dataService: SessionService,
    private router: Router
  ) {
    // dataService.getLoggedInName.subscribe(name => this.changeName(name));
    if (this.dataService.isLoggedIn()) {
      if(this.router.url == '/login'){
				console.log(this)
				this.router.navigate(['home']);
      }
      // this.currentUser = localStorage.getItem('currentUser')? JSON.parse(localStorage.getItem('currentUser')) : '';
      // console.log(this.currentUser);
      
      this.loginbtn=false;
      this.logoutbtn=true
    }
    else {
      this.loginbtn=true;
      this.logoutbtn=false
    }
    
  }
  private changeName(name: boolean): void {
    this.logoutbtn = name;
    this.loginbtn = !name;
  }

  logout() {
    this.dataService.deleteToken();
    window.location.href = window.location.href;
  }
}
