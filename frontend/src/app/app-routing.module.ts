import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './component/products/products.component';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { NotAuthorizedComponent } from './component/error/not-authorized/not-authorized.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layout/admin-layout/admin-layout.module#AdminLayoutModule'
      }]
  },
  { path: 'php-angular', component: ProductsComponent },

  { path: 'admin/login', component: LoginComponent },
  { path: 'reviewer/login', component: LoginComponent },
  { path: 'researcher/login', component: LoginComponent },

  { path: 'admin/registration', component: RegistrationComponent },
  { path: 'reviewer/registration', component: RegistrationComponent },
  { path: 'researcher/registration', component: RegistrationComponent },

  { path: 'not-authorized', component: NotAuthorizedComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
