<?php
	if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
		include 'database.php';
		$products = [];

		$sql = "SELECT * FROM products WHERE id = ?";
		// print_r($stmt);die;
		if($stmt = mysqli_prepare($db, $sql)){
			// Bind variables to the prepared statement as parameters
			mysqli_stmt_bind_param($stmt, "i", $param_id);
			
			// Set parameters
			$param_id = trim($_GET["id"]);
			
			// Attempt to execute the prepared statement
			if (mysqli_stmt_execute($stmt)) {
				$result = mysqli_stmt_get_result($stmt);
		
				if (mysqli_num_rows($result) == 1) {
					/* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					
					// Retrieve individual field value
					$id= $row['id'];
					$name= $row['name'];
					$price = $row['price'];

					$data = array('id' => $id, 'name' => $name, 'price' => $price);
					echo json_encode($data);
				} 
				else {
					print_r('Invalid route!');die;
				}
				
			} 
			else {
				echo "Oops! Something went wrong. Please try again later.";
			}
		} 
		else {
			http_response_code(404);
		}
	}
	else { 
		// URL doesn't contain id parameter. Redirect to error page
		print_r('error');die;
	}