<?php
require_once('ClassParent.php');
class AutoAssignResearchPaperToReviewer extends ClassParent {

    public function checkReviewerIdNotNull($index){
        $query1 = <<<EOT
            SELECT
            *
            FROM research_papers
            WHERE research_paper_status = 'Pending'
            AND reviewer_id IS NULL
            ORDER BY id asc;
EOT;
        $researchPaperquery = pg_query($query1);
        $return=array();
        if(pg_numrows($researchPaperquery)){
            $return['status'] = true;
            $return['msg'] = "Success";
            $return['sql'] = $query1;
            $return['result']=array();
            while($row = pg_fetch_assoc($researchPaperquery)){
                $return['result'][] = $row;
            }
            $return['research_paper'] = $return['result'][$index];
        }else{
            $return['status'] = false;
            $return['msg'] = pg_last_error();
            $return['result'] = 'wala';
        }
        pg_free_result($researchPaperquery);
        $this->research_paper_id = intval($return['research_paper']['id']);
        $this->research_paper_tag = intval($return['research_paper']['tag_id']);
        echo ('research_paper_id:' . ' ' . $this->research_paper_id . "\n");
        print_r('tagId:' . ' ' . $this->research_paper_tag . "\n");

        $query2 = <<<EOT
            SELECT 
            users.id,
            users.specialization,
            COUNT(research_papers.reviewer_id)
            FROM (research_papers
            RIGHT JOIN users ON research_papers.reviewer_id = users.id)
            WHERE 
            users.user_type = 3 
            AND users.specialization = $this->research_paper_tag
            AND research_papers.research_paper_status != 'Done'
            AND
            (
                SELECT
                (
                    CASE 
                        WHEN (
                                SELECT 
                                count(*) 
                                FROM research_papers 
                                where research_papers.reviewer_id = users.id
                            ) > 0
                            THEN (
                                SELECT 
                                count(*) 
                                FROM research_papers 
                                where research_papers.reviewer_id = users.id
                            )
                    ELSE 0
                    END
                ) 
            ) < 5
            GROUP BY users.id
            ORDER BY COUNT(research_papers.reviewer_id) asc;

EOT;

        $vacantReviewerquery = pg_query($query2);
        $returnValueVacant=array();
        if(pg_numrows($vacantReviewerquery)){
            $returnValueVacant['status'] = true;
            $returnValueVacant['msg'] = "Success";
            $returnValueVacant['result']=array();
            while($row = pg_fetch_assoc($vacantReviewerquery)){
                $returnValueVacant['result'][] = $row;
            }
            $returnValueVacant['reviewer'] = $returnValueVacant['result'][0];
        }else{
            $returnValueVacant['status'] = false;
            $returnValueVacant['msg'] = pg_last_error();
        }
        pg_free_result($vacantReviewerquery);

        $this->reviewer_id = intval($returnValueVacant['reviewer']['id']);
        print_r('reviewer_id:' . ' ' .  $this->reviewer_id. "\n\n");
    }

    public function setAutoMatically() {
        $index = 0;
        $research_paper_id;
        $research_paper_tag;
        $reviewer_id;

        $this->checkReviewerIdNotNull($index);

        print_r('tagId2:' . ' ' . $this->research_paper_tag . "\n");
        
        while($this->reviewer_id == 0){
            $this->index = $this->index+1;
            $this->checkReviewerIdNotNull($this->index);
        }
        
        $status = 'In-Review';


        $sql = <<<EOT
        UPDATE research_papers SET
        ( 
            reviewer_id,
            research_paper_status
        ) 
        = 
        ( 
            $this->reviewer_id,
            '$status'
        )
        WHERE id = $this->research_paper_id;
EOT;
        return ClassParent::update($sql);
    }
}
?>