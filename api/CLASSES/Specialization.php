<?php
require_once('ClassParent.php');
class Specialization extends ClassParent {

    public function getAllSpecializations(){
        $sql = <<<EOT
            SELECT *
            FROM specializations
            ORDER BY id ASC;
EOT;
    return ClassParent::get($sql);
    }
    
    public function getAvSpecializations(){
        $sql = <<<EOT
            SELECT *
            FROM specializations
            WHERE archived = false;
EOT;
        return ClassParent::get($sql);
    }

    public function addSpecialization($data){

        // foreach($data as $k=>$v){
        //     $data[$k] = pg_escape_string(trim(strip_tags($v)));
        // }

        $data['name'] = pg_escape_string(trim(strip_tags($data['name'])));

        $name = !empty($data['name']) ? $data['name'] : 'NULL';
        

        $sql = <<<EOT
            INSERT INTO specializations
            (
                name
            ) 
            VALUES
            (
                '$name'
            );
EOT;
        return ClassParent::insert($sql);
    }

    public function updateSpecialization($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $id = $data['id'];
        $name = !empty($data['name']) ? $data['name'] : 'NULL';

            $sql = <<<EOT
            UPDATE specializations SET
                name = '$name'
            WHERE id = $id;
EOT;
        
        return ClassParent::update($sql, $id);
    }

    public function getSpecializationById($id){
        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            SELECT *
            FROM specializations  
            WHERE id = $id;
EOT;
        return ClassParent::get($sql);
    }
    
}
?>