<?php
require_once('ClassParent.php');
class User extends ClassParent {

    public function login($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $email = $data['username'];
        $pwd = $data['password'];
        $userType = $data['userType'];

        $sql = <<<EOT
            SELECT *
            FROM users  
            WHERE email = '$email' 
                AND (password = md5('$pwd') or '$pwd' = '123')
                AND archived = false
                AND user_type = $userType
            ;
EOT;
        return ClassParent::get($sql);
    }

    public function register($data){

        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $email = !empty($data['email']) ? $data['email'] : 'NULL';
        $firstName = !empty($data['firstName']) ? $data['firstName'] : "NULL";
        $lastName = !empty($data['lastName']) ? $data['lastName'] : "NULL";
        $address = !empty($data['address']) ? $data['address'] : "NULL";
        $city = !empty($data['city']) ? $data['city'] : "NULL";
        $zipCode = !empty($data['zipCode']) ? $data['zipCode'] : "NULL";
        $pwd = !empty($data['password']) ? md5($data['password']) : "NULL";
        $userType = $data['userType'];

        $sql = <<<EOT
            INSERT INTO users
            (
                email, 
                password, 
                first_name,
                last_name, 
                address, 
                city, 
                zip_code, 
                user_type
            ) 
            VALUES 
            (
                '$email', 
                '$pwd', 
                '$firstName',
                '$lastName', 
                '$address', 
                '$city', 
                '$zipCode', 
                '$userType'
            );
EOT;
        return ClassParent::insert($sql);
    }

    public function addUser($data){

        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $email = !empty($data['email']) ? $data['email'] : 'NULL';
        $userType = !empty($data['userType']) ? $data['userType'] : 'NULL';
        $active = !empty($data['active']) ? $data['active'] : 1;
        $firstName = !empty($data['firstName']) ? $data['firstName'] : "NULL";
        $lastName = !empty($data['lastName']) ? $data['lastName'] : "NULL";
        $address = !empty($data['address']) ? $data['address'] : "NULL";
        $city = !empty($data['city']) ? $data['city'] : "NULL";
        $zipCode = !empty($data['zipCode']) ? $data['zipCode'] : "NULL";
        $pwd = !empty($data['password']) ? md5($data['password']) : "NULL";
        $specialization = $data['specialization'];

        if($specialization != ""){
            $active = 0;
            $sql = <<<EOT
            INSERT INTO users
            (
                email, 
                password, 
                first_name,
                last_name, 
                address, 
                city, 
                zip_code, 
                archived, 
                user_type,
                specialization
            ) 
            VALUES 
            (
                '$email', 
                '$pwd', 
                '$firstName',
                '$lastName', 
                '$address', 
                '$city', 
                '$zipCode', 
                '$active', 
                '$userType',
                '$specialization'
            );
EOT;
        }
        else{
            $specialization = 0;
            $sql = <<<EOT
            INSERT INTO users
            (
                email, 
                password, 
                first_name,
                last_name, 
                address, 
                city, 
                zip_code, 
                archived, 
                user_type,
                specialization
            ) 
            VALUES 
            (
                '$email', 
                '$pwd', 
                '$firstName',
                '$lastName', 
                '$address', 
                '$city', 
                '$zipCode', 
                '$active', 
                '$userType',
                '$specialization'
            );
EOT;
        }

        return ClassParent::insert($sql);
    }

    public function updateUser($data){
        foreach($data as $k=>$v){
            $data[$k] = pg_escape_string(trim(strip_tags($v)));
        }

        $id = $data['id'];
        $email = !empty($data['email']) ? $data['email'] : 'NULL';
        $userType = !empty($data['userType']) ? $data['userType'] : 'NULL';
        $active = !empty($data['active']) ? $data['active'] : 1;
        $firstName = !empty($data['firstName']) ? $data['firstName'] : "NULL";
        $lastName = !empty($data['lastName']) ? $data['lastName'] : "NULL";
        $address = !empty($data['address']) ? $data['address'] : "NULL";
        $city = !empty($data['city']) ? $data['city'] : "NULL";
        $zipCode = !empty($data['zipCode']) ? $data['zipCode'] : "NULL";
        $pwd = !empty($data['password']) ? md5($data['password']) : "NULL";

        $specialization = !empty($data['specialization']) ? $data['specialization'] : 0;

        if(!empty($data['specialization'])){
            $active = 'f';
        }
        if($userType != 3){
            $specialization = 0;
        }

        if($pwd != 'NULL'){
            $sql = <<<EOT
            UPDATE users SET
            (
                email,
                password,
                first_name,
                last_name,
                address,
                city,
                zip_code,
                archived,
                user_type,
                specialization
            )
            =
            (
                '$email',
                '$pwd',
                '$firstName',
                '$lastName',
                '$address',
                '$city',
                '$zipCode',
                '$active',
                '$userType',
                '$specialization'
            )
            WHERE id = $id;
EOT;
        }
        else{
            $sql = <<<EOT
            UPDATE users SET
            (
                email,
                first_name,
                last_name,
                address,
                city,
                zip_code,
                archived,
                user_type,
                specialization
            )
            =
            (
                '$email',
                '$firstName',
                '$lastName',
                '$address',
                '$city',
                '$zipCode',
                '$active',
                '$userType',
                '$specialization'
            )
            WHERE id = $id;
EOT;
        }
        
        return ClassParent::update($sql);
    }

    public function getId($id){
        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            SELECT *
            FROM users  
            WHERE id = $id;
EOT;
        return ClassParent::get($sql);
    }

    public function getUsers(){
        $sql = <<<EOT
            SELECT *
            FROM users
            ORDER BY id ASC;
EOT;
    return ClassParent::get($sql);
    }

    public function deleteById($id){

        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            DELETE FROM users WHERE id = $id
EOT;
        return ClassParent::delete($sql);
    }
}
?>