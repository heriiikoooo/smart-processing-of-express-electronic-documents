<?php
error_reporting(E_ERROR);
class ClassParent {

    protected function get($sql){
        $query = pg_query($sql);
        $return=array();
        if(pg_numrows($query)){
            $return['status'] = true;
            $return['msg'] = "Success";
            $return['sql'] = $sql;
            $return['result']=array();
            while($row = pg_fetch_assoc($query)){
                $return['result'][] = $row;
            }
            // $return['count'] = $reviewer;
        }else{
            $return['status'] = false;
            $return['sql'] = $sql;
            $return['msg'] = pg_last_error();
            $return['result'] = NULL;
        }

        pg_free_result($query);
        return $return;
    }

    protected function update($sql){
        $query = pg_query($sql);
        $return=array();

        if($query){
            $return['status'] = true;
            $return['msg'] = "Success";
        }else{
            $return['status'] = false;
            $return['msg'] = pg_last_error();
        }

        pg_free_result($query);
        return $return;
    }
    protected function delete($sql){
        $query = pg_query($sql);
        $return=array();

        if($query){
            $return['status'] = true;
            $return['msg'] = "Success";
        }else{
            $return['status'] = false;
            $return['msg'] = pg_last_error();
        }

        pg_free_result($query);
        return $return;
    }

    protected function insert($sql){
        $query = pg_query($sql);
        $return=array();

        if($query){
            $return['status'] = true;
            $return['msg'] = "Success";
            $return['returning'] = pg_fetch_assoc($query);
        }else{
            $return['status'] = false;
            $return['msg'] = pg_last_error();
        }

        pg_free_result($query);
        return $return;
    }
}

?>