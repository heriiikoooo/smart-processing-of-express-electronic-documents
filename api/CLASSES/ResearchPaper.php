<?php
require_once('ClassParent.php');
class ResearchPaper extends ClassParent {


    public function addPaper($data){

        // foreach($data as $k=>$v){
        //     $data[$k] = pg_escape_string(trim(strip_tags($v)));
        // }

       
        $title = !empty($data['title']) ? $data['title'] : 'NULL';
        $body = !empty($data['body']) ? $data['body'] : 'NULL';
        $tags = !empty($data['tags']) ? $data['tags'] : 'NULL';
        $userID = !empty($data['userID']) ? $data['userID'] : 'NULL';
        $status = !empty($data['status']) ? $data['status'] : 'NULL';
        

        $sql = <<<EOT
            INSERT INTO research_papers
            (
                research_paper_name, 
                body, 
                tag_id,
                user_id, 
                research_paper_status
            ) 
            VALUES 
            (
                '$title', 
                '$body', 
                '$tags',
                '$userID', 
                '$status'
            );
EOT;
        return ClassParent::insert($sql);
    }

    public function updatePaper($data){

        // foreach($data as $k=>$v){
        //     $data[$k] = pg_escape_string(trim(strip_tags($v)));
        // }
        $id = $data['id'];
        $title = !empty($data['title']) ? $data['title'] : 'NULL';
        $body = !empty($data['body']) ? $data['body'] : 'NULL';
        $tags = !empty($data['tags']) ? $data['tags'] : 'NULL';
        $userID = !empty($data['userID']) ? $data['userID'] : 'NULL';
        $status = !empty($data['status']) ? $data['status'] : NULL;
        

        $sql = <<<EOT
            UPDATE research_papers SET
            (
                research_paper_name, 
                body, 
                tag_id,
                user_id, 
                research_paper_status
            ) 
            = 
            (
                '$title', 
                '$body', 
                '$tags',
                '$userID', 
                '$status'
            )
            WHERE id = $id;
EOT;
        return ClassParent::update($sql);
    }

    public function deletePaperById($id){

        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            DELETE FROM research_papers WHERE id = $id
EOT;
        return ClassParent::delete($sql);
    }

    public function getAllResearchPapers() {
        $sql = <<<EOT
        SELECT research_papers.*, users.first_name, users.last_name
        FROM research_papers LEFT JOIN users 
        ON research_papers.user_id = users.id
        ORDER BY id ASC;
EOT;
        return ClassParent::get($sql);
    }

    public function getVacantReviewers() {
        $sql = <<<EOT
                SELECT 
                users.id,
                users.specialization,
                COUNT(research_papers.reviewer_id)
                FROM (research_papers
                INNER JOIN users ON research_papers.reviewer_id = users.id)
                WHERE 
                users.user_type = 3 
                AND users.specialization = 2
                AND
                (
                    SELECT
                    (
                        CASE 
                            WHEN (
                                    SELECT 
                                    count(*) 
                                    FROM research_papers 
                                    where research_papers.reviewer_id = users.id
                                ) > 0
                                THEN (
                                    SELECT 
                                    count(*) 
                                    FROM research_papers 
                                    where research_papers.reviewer_id = users.id
                                )
                        ELSE 0
                        END
                    ) 
                ) < 5
                GROUP BY users.id
                ORDER BY COUNT(research_papers.reviewer_id) asc;

EOT;
        return ClassParent::get($sql);
    }

    public function getAllResearchPapersOnAuto() {
        $sql = <<<EOT
            SELECT
            *
            FROM research_papers
            WHERE research_paper_status = 'Pending'
            AND reviewer_id IS NULL
            ORDER BY id asc;
EOT;
        return ClassParent::get($sql);
    }

    public function getOwnResearchPapers($id) {
        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            SELECT research_papers.*, users.first_name, users.last_name
            FROM research_papers LEFT JOIN users 
            ON research_papers.user_id = users.id 
            WHERE research_papers.user_id = $id
            ORDER BY id ASC;
EOT;

        return ClassParent::get($sql);
    }

    public function getResearchPaperById($id) {
        
        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            SELECT * FROM research_papers
            WHERE id = $id;
EOT;
        return ClassParent::get($sql);
    }


    public function setReviewer($data){

        // foreach($data as $k=>$v){
        //     $data[$k] = pg_escape_string(trim(strip_tags($v)));
        // }
        $id = $data['id'];
        $reviewer_id = !empty($data['reviewer_id']) ? $data['reviewer_id'] : 'NULL';
        $status = 'In-Review';
        

        $sql = <<<EOT
            UPDATE research_papers SET
            ( 
                research_paper_status,
                reviewer_id
            ) 
            = 
            ( 
                '$status',
                '$reviewer_id'
            )
            WHERE id = $id;
EOT;
        return ClassParent::update($sql);
    }

}
?>