<?php
require_once('ClassParent.php');
class Reviewer extends ClassParent {

    public function getAllAssignedResearchPapers($id) {
        
        $id = pg_escape_string(trim(strip_tags($id)));

        $sql = <<<EOT
            SELECT research_papers.*, users.first_name, users.last_name
            FROM research_papers LEFT JOIN users 
            ON research_papers.reviewer_id = users.id 
            WHERE research_papers.reviewer_id = $id
            ORDER BY id ASC;
EOT;
        return ClassParent::get($sql);
    }

    public function getActiveReviewers() {
        $sql = <<<EOT
            SELECT * FROM users
            WHERE user_type = 3
            AND archived = false;
EOT;
    return ClassParent::get($sql);
    }

}
?>