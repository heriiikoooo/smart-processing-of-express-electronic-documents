CREATE USER spoeed WITH PASSWORD 'spoeed_password';

CREATE DATABASE system_db;

DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id serial primary key,
    email varchar NOT NULL CONSTRAINT non_empty_email CHECK(length(email)>0),
    password varchar default md5('user123456'),
    first_name varchar NOT NULL CONSTRAINT non_empty_first_name CHECK(length(first_name)>0),
    last_name varchar NOT NULL CONSTRAINT non_empty_last_name CHECK(length(last_name)>0),
    address varchar NOT NULL CONSTRAINT non_empty_address CHECK(length(address)>0),
    city varchar NOT NULL CONSTRAINT non_empty_city CHECK(length(city)>0),
    zip_code int NOT NULL,
    tsv tsvector,
    archived boolean default false,
    specialization integer CONSTRAINT specialization FOREIGN KEY (specialization) REFERENCES specializations (id)
);
ALTER TABLE users OWNER TO spoeed;

CREATE INDEX tsv_idx ON users USING gin(tsv);

DROP FUNCTION IF EXISTS users_search_trigger();
CREATE FUNCTION users_search_trigger() RETURNS trigger AS $$
begin
  new.tsv :=
    setweight(to_tsvector(coalesce(new.first_name,'')), 'A') ||
    setweight(to_tsvector(coalesce(new.last_name,'')), 'A') ||
    setweight(to_tsvector(coalesce(new.email,'')), 'B');
  return new;
end
$$ LANGUAGE plpgsql;

CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
ON users FOR EACH ROW EXECUTE PROCEDURE users_search_trigger();

INSERT INTO users
(
    email,
    first_name,
    last_name,
    password,
    address,
    city,
    zip_code
)
VALUES
(
    'emai@email.com',
    'Email',
    'Man',
    '4da49c16db42ca04538d629ef0533fe8',
    'Address',
    'Manila',
    1550
);

DROP TABLE IF EXISTS specializations;
CREATE TABLE specializations
(
  id serial primary key,
  name varchar NOT NULL UNIQUE CONSTRAINT non_empty_name CHECK(length(name)>0),
  archived boolean default false,
  date_created timestamp default now()
);
ALTER TABLE specializations OWNER TO spoeed;


INSERT INTO specializations
(
    id,
    name
)
VALUES
(
    0,
    ' '
);


CREATE TABLE tags
(
  id serial primary key,
  name varchar NOT NULL UNIQUE CONSTRAINT non_empty_name CHECK(length(name)>0),
  archived boolean default false,
  date_created timestamp default now()
);
ALTER TABLE tags OWNER TO jericho;


INSERT INTO tags
(
    id,
    name
)
VALUES
(
    0,
    ' '
);

