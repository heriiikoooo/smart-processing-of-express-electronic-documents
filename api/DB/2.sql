--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: users_search_trigger(); Type: FUNCTION; Schema: public; Owner: posgres
--

CREATE FUNCTION public.users_search_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.tsv :=
    setweight(to_tsvector(coalesce(new.first_name,'')), 'A') ||
    setweight(to_tsvector(coalesce(new.last_name,'')), 'A') ||
    setweight(to_tsvector(coalesce(new.email,'')), 'B');
  return new;
end
$$;


ALTER FUNCTION public.users_search_trigger() OWNER TO posgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: research_papers; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE public.research_papers (
    id integer NOT NULL DEFAULT nextval('research_paperss_id_seq'::regclass),
    research_paper_name character varying NOT NULL,
    body text NOT NULL,
    research_paper_status character varying NOT NULL,
    user_id integer NOT NULL,
    tags jsonb,
    reviewer_id integer,
    CONSTRAINT research_paperss_pkey PRIMARY KEY (id),
    CONSTRAINT reviewer_id FOREIGN KEY (reviewer_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_id FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT non_empty_body CHECK (length(body) > 0),
    CONSTRAINT non_empty_research_paper_name CHECK (length(research_paper_name::text) > 0),
    CONSTRAINT non_empty_research_paper_status CHECK (length(research_paper_status::text) > 0)
);


ALTER TABLE public.research_papers OWNER TO posgres;

--
-- Name: research_papers_id_seq; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE public.research_papers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.research_papers_id_seq OWNER TO posgres;

--
-- Name: research_papers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: posgres
--

ALTER SEQUENCE public.research_papers_id_seq OWNED BY public.research_papers.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: jericho
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying NOT NULL,
    password character varying DEFAULT md5('user123456'::text),
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    address character varying NOT NULL,
    city character varying NOT NULL,
    zip_code integer NOT NULL,
    tsv tsvector,
    archived boolean DEFAULT false,
    user_type integer NOT NULL,
    active boolean DEFAULT false,
    CONSTRAINT non_empty_address CHECK ((length((address)::text) > 0)),
    CONSTRAINT non_empty_city CHECK ((length((city)::text) > 0)),
    CONSTRAINT non_empty_email CHECK ((length((email)::text) > 0)),
    CONSTRAINT non_empty_first_name CHECK ((length((first_name)::text) > 0)),
    CONSTRAINT non_empty_last_name CHECK ((length((last_name)::text) > 0))
);


ALTER TABLE public.users OWNER TO jericho;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: jericho
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO jericho;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jericho
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: research_papers id; Type: DEFAULT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY public.research_papers ALTER COLUMN id SET DEFAULT nextval('public.research_papers_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: research_papers; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY public.research_papers (id, research_paper_name, research_paper_details, research_paper_status, user_id) FROM stdin;
2	FilEngKo	Filipino, English and Korean Translator	Pending	2
3	Star Tracker	A Astrophotography research paper to find stars easily	In-Review	10
6	Time Machine	A time machine	Done	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: jericho
--

COPY public.users (id, email, password, first_name, last_name, address, city, zip_code, tsv, archived, user_type, active) FROM stdin;
10	test@mail.com	0192023a7bbd73250516f069df18b500	test	tesxt	test	tesxt	123	'test':1A 'test@mail.com':3B 'tesxt':2A	f	2	f
13	testr@mail.com	0192023a7bbd73250516f069df18b500	asas	asas	aasa	sasas	123	'asa':1A,2A 'testr@mail.com':3B	f	3	f
2	admin@thesis.com	0192023a7bbd73250516f069df18b500	Jericho	Manalo	Balayan	Batangas	4213	'admin@thesis.com':3B 'jericho':1A 'manalo':2A	t	1	t
\.


--
-- Name: research_papers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('public.research_papers_id_seq', 6, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jericho
--

SELECT pg_catalog.setval('public.users_id_seq', 13, true);


--
-- Name: research_papers research_papers_pkey; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY public.research_papers
    ADD CONSTRAINT research_papers_pkey PRIMARY KEY (id);


--
-- Name: users users_address_key; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_address_key UNIQUE (address);


--
-- Name: users users_city_key; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_city_key UNIQUE (city);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_first_name_key; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_first_name_key UNIQUE (first_name);


--
-- Name: users users_last_name_key; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_last_name_key UNIQUE (last_name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: jericho
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: fki_user_id; Type: INDEX; Schema: public; Owner: posgres
--

CREATE INDEX fki_user_id ON public.research_papers USING btree (user_id);


--
-- Name: tsv_idx; Type: INDEX; Schema: public; Owner: jericho
--

CREATE INDEX tsv_idx ON public.users USING gin (tsv);


--
-- Name: users tsvectorupdate; Type: TRIGGER; Schema: public; Owner: jericho
--

CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.users_search_trigger();


--
-- Name: research_papers user_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY public.research_papers
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

