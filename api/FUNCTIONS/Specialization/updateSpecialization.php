<?php
    require_once('../../database.php');
    require_once('../../CLASSES/Specialization.php');

    $data = array();
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata, true);

    if($_SERVER['REQUEST_METHOD'] == 'PUT'){
        $class = new Specialization();
        $result = $class->updateSpecialization($data);
    }



    header("Content-Type: application/json");
    print(json_encode($result));
?>