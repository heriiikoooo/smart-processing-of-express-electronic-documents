<?php
require_once('../../database.php');
require_once('../../CLASSES/Specialization.php');

$data = array();
$id = trim($_GET["id"]);

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $class = new Specialization();
    $result = $class->getSpecializationById($id);
}



header("Content-Type: application/json");
print(json_encode($result));
?>