<?php
    require_once('../../database.php');
    require_once('../../CLASSES/Specialization.php');
    
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $class = new Specialization();
        $result = $class->getAvSpecializations();
    }

    header("Content-Type: application/json");
    print(json_encode($result));
?>