<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

error_reporting(E_ERROR);
$tempPath = $_FILES['upload']['tmp_name'];

$newFileName = randomPrefix(15).".".end(explode(".", $_FILES['upload']['name']));
$additionaldir = date('Ymd');

$dir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '../../ASSETS/UPLOADS/' . $additionaldir;
if (!is_dir('../../ASSETS/UPLOADS/' . $additionaldir)) {
	mkdir('../../ASSETS/UPLOADS/' . $additionaldir, 0777);
}   
$uploadPath = $dir . DIRECTORY_SEPARATOR . $newFileName;

$weblink = 'http://localhost/maam-kim-thesis/api/';

$a = move_uploaded_file( $tempPath, $uploadPath );

if($a){
	$answer = array( 'url' => $weblink . 'ASSETS/UPLOADS/' . $additionaldir . DIRECTORY_SEPARATOR . $newFileName );
} else {
	$answer = array( 'url' => '' );
}

echo $json = json_encode( $answer );

function randomPrefix($length){
	$random= "";
	srand((double)microtime()*1000000);

	$data = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for($i = 0; $i < $length; $i++){
		$random .= substr($data, (rand()%(strlen($data))), 1);
	}

	return $random;
}
?>