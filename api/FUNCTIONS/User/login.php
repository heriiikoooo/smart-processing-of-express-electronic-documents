<?php
require_once('../../database.php');
require_once('../../CLASSES/User.php');

$data = array();
$postdata = file_get_contents("php://input");
$data = json_decode($postdata, true);

$class = new User();
$result = $class->login($data);

header("Content-Type: application/json");
print(json_encode($result));
?>