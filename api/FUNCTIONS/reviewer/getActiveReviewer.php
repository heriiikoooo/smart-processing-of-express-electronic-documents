<?php
    require_once('../../database.php');
    require_once('../../CLASSES/Reviewer.php');

    $class = new Reviewer();
    $result = $class->getActiveReviewers();

    header("Content-Type: application/json");
    print(json_encode($result));
?>