<?php
    require_once('../../database.php');
    require_once('../../CLASSES/Reviewer.php');

    $data = array();
    $id = trim($_GET["id"]);

    $class = new Reviewer();
    $result = $class->getAllAssignedResearchPapers($id);

    header("Content-Type: application/json");
    print(json_encode($result));
?>