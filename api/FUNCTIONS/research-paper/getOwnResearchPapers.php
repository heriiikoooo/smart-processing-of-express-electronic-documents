<?php
require_once('../../database.php');
require_once('../../CLASSES/ResearchPaper.php');

$data = array();
$id = trim($_GET["id"]);

$class = new ResearchPaper();
$result = $class->getOwnResearchPapers($id);



header("Content-Type: application/json");
print(json_encode($result));
?>