<?php
    require_once('../../database.php');
    require_once('../../CLASSES/ResearchPaper.php');

    $class = new ResearchPaper();
    $result = $class->getAllResearchPapers();

    header("Content-Type: application/json");
    print(json_encode($result));
?>