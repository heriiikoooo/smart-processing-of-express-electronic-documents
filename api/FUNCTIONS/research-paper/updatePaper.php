<?php
require_once('../../database.php');
require_once('../../CLASSES/ResearchPaper.php');

$data = array();
$postdata = file_get_contents("php://input");
$data = json_decode($postdata, true);

$class = new ResearchPaper();
$result = $class->updatePaper($data);



header("Content-Type: application/json");
print(json_encode($result));
?>